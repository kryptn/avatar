from django.db import models
from evelink.api import API, APIError
from evelink.account import Account
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError



class Key(models.Model):
    user = models.ForeignKey(User)
    keyID = models.CharField(max_length=64)
    vCode = models.CharField(max_length=124)
    accessMask = models.IntegerField(null=True)
    keyType = models.CharField(max_length=20,null=True)
    #queries = models.ForeignKey(Query)

    def clean(self):
        try:
            api = API(api_key=(self.keyID,self.vCode))
            result = Account(api=api).key_info()
            self.accessMask = result['access_mask']
            self.keyType = result['type']
            assert result['expire_ts'] == None
        except APIError:
            raise ValidationError('Invalid keyID or vCode')
        except AssertionError:
            raise ValidationError('Key expires, use a non-expiring key')

class Party(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=256,null=True)

    def __unicode__(self):
        if self.name:
            return unicode(self.name)
        else:
            return unicode(self.id)

class Character(models.Model):
    key = models.ForeignKey(Key)
    

class Query(models.Model):
    key = models.ManyToManyField(Key)
    requestTime = models.DateTimeField()
    response = models.CharField(max_length=256)
    







