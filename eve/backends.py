from django.contrib.auth.models import User, check_password
from hashlib import sha1
import requests
import logging

logger = logging.getLogger(__name__)
print 'test'
class TESTAuthBackend(object):

    def api(self, user, passw):
        apiuri = 'https://auth.pleaseignore.com/api/1.0/login'
        payload = {'user':user, 'pass':sha1(passw).hexdigest()}
        result = requests.get(apiuri, params=payload)
        return result.json()

    def authenticate(self, username=None, password=None):
        try:
            user = User.objects.get(username=username)
            print 'user exists'
            if check_password(password, user.password):
                print 'password correct'
                return user
            else:
                print 'password incorrect'
                r = self.api(username, password)
                if r['auth'] == 'ok':
                    print 'auth revalidated'
                    user.set_password(password)
                    user.save()
                    return user
        except User.DoesNotExist:
            print 'user does not exist'
            r = self.api(username, password)
            if r['auth'] == 'ok':
                print 'auth verified'
                user = User(username=username, password=password)
                user.save()
                return user
        return None

    def get_user(self, user_id):
        try:
            print 'userid grab?'
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            print 'but none'
            return None