from django.contrib import admin
from eve.models import Key, Party

class KeyAdmin(admin.ModelAdmin):
    fields = ['user','keyID','vCode']
    list_display = ['keyID','vCode','accessMask','keyType','user']

class PartyAdmin(admin.ModelAdmin):
    list_display = ['id','name']

admin.site.register(Key, KeyAdmin)
admin.site.register(Party, PartyAdmin)