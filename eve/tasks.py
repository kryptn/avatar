from celery import task
import evelink
from evelink.api import APIError
from eve.models import Key, Party


@task()
def populate_parties():
    r = Party.objects.filter(name=None)
    unknown = [x.id for x in r]
    api = evelink.eve.EVE()

    for u in unknown:
        try:
            result = api.character_name_from_id(u)
            p = Party.objects.get(id=u)
            p.name = result
            p.save()
        except APIError:
            pass

