from django.db import models
from evelink.api import API, APIError
from evelink.account import Account
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from eve.models import Key, Party
from datetime import datetime, timedelta

class Job(models.Model):
    job_id = models.IntegerField(primary_key=True)
    line_id = models.IntegerField()
    container_id = models.IntegerField()
    input_id = models.IntegerField()
    input_blueprint_type = models.CharField(max_length=15)
    input_location_id = models.ForeignKey(Party, related_name='input_location_id')
    input_quantity = models.IntegerField()
    input_prod_level = models.IntegerField()
    input_mat_level = models.IntegerField()
    input_runs_left = models.IntegerField()
    input_item_flag = models.IntegerField()
    input_type_id = models.ForeignKey(Party, related_name='input_type_id')
    output_location_id = models.ForeignKey(Party, related_name='output_location_id')
    output_bpc_runs = models.IntegerField()
    output_container_location_id = models.ForeignKey(Party, related_name='output_container_location_id')
    output_type_id = models.ForeignKey(Party, related_name='output_type_id')
    output_flag = models.IntegerField()
    runs = models.IntegerField()
    installer_id = models.ForeignKey(Party, related_name='installer_id')
    system_id = models.ForeignKey(Party, related_name='system_id')
    multipliers_material = models.FloatField()
    multipliers_char_material = models.FloatField()
    multipliers_time = models.FloatField()
    multipliers_char_time = models.FloatField()
    container_type_id = models.ForeignKey(Party, related_name='container_type_id')
    delivered = models.BooleanField()
    finished = models.BooleanField()
    status = models.CharField(max_length=20)
    activity_id = models.IntegerField()
    install_ts = models.IntegerField()
    begin_ts = models.IntegerField()
    end_ts = models.IntegerField()
    pause_ts = models.IntegerField(null=True)

    def install_time(self):
        return datetime.utcfromtimestamp(self.install_ts)

    def begin_time(self):
        return datetime.utcfromtimestamp(self.begin_ts)

    def end_time(self):
        return datetime.utcfromtimestamp(self.end_ts)

    def time_left(self):
        return datetime.utcfromtimestamp(self.end_ts)-datetime.utcnow()



