from django.contrib import admin
from industry.models import Job

class JobAdmin(admin.ModelAdmin):
    list_display = ['job_id',
                    'output_location_id',
                    'output_type_id',
                    'installer_id',
                    'system_id',
                    'delivered',
                    'end_time',
                    'time_left']

    ordering = ('end_ts',)

    list_filter = ('delivered',)

admin.site.register(Job, JobAdmin)
