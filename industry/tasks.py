from celery import task
from industry.models import Job
from eve.models import Key, Party
import evelink
from datetime import datetime

@task()
def update_jobs(keyID,vCode):
    api = evelink.api.API(api_key=(keyID,vCode))
    corp = evelink.corp.Corp(api=api)
    result = corp.industry_jobs()
    r = {}

    for key, value in result.iteritems():

        r = {'job_id': key}
        for k, v in value.iteritems():

            if type(v) is dict:
                for x, y in v.iteritems():
                    r[k+'_'+x] = y
            else:
                r[k] = v

        parties = ['input_location_id',
                   'input_type_id',
                   'output_location_id',
                   'output_container_location_id',
                   'output_type_id',
                   'installer_id',
                   'system_id',
                   'container_type_id']

        for field in parties:
            p = Party(r[field])
            p.save()
            r[field] = p


        job = Job(**r)
        job.save()

@task()
def update_all_jobs():
    keys = Key.objects.all()
    for key in keys:
        update_jobs(key.keyID, key.vCode)
